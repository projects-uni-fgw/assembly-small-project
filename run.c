#include <stdio.h>
#include <stdlib.h>

int main(void) {
    // Delare variables here (C syntax)
    char questionMSG1[] = "How many numbers: ";
    char cStore[] = "%d";
    int counter = 0;
    char numStore[] = "%d";
    int num = 0;
    char questionMSG2[] = "\nEnter a number: ";
    char line[] = "\n----------------- \n";
    char positiveMSG[] = "Positive: %d\n";
    char negativeMSG[] = "Negative: %d\n";
    char zeroMSG[] = "Zero: %d";
    int sumPositive = 0;
    int sumZeros = 0;
    int sumNegative = 0;

    _asm {
        // Output text
        lea eax, questionMSG1
        push eax
        call printf
        pop eax
        // Input counter
        lea eax, counter
        push eax // Push address of counter
        lea eax, cStore
        push eax// Push address of cStore
        call scanf
        add esp, 8
        
        mov ecx, counter // set count value to ecx

        looper :
        // Enter numbers question
        push ecx
        lea eax, questionMSG2
        push eax
        call printf
        pop eax
        // User's input numbers
        lea eax, num
        push eax
        lea eax, numStore
        push eax
        call scanf
        add esp, 8
        // Checker
        pop ecx
        mov eax, num
        cmp eax, 0
        jl isNegative
        jnz isPositive
        jz isZero
        loop looper
        
        // In three cases, increment sum by 1.
        isPositive :
            mov ebx, sumPositive
            inc ebx
            mov sumPositive, ebx
            loop looper
            jmp results
        isZero :
            mov ebx, sumZeros
            inc ebx
            mov sumZeros, ebx
            loop looper
            jmp results
        isNegative :
            mov ebx, sumNegative
            inc ebx
            mov sumNegative, ebx
            loop looper
            jmp results
        
        // Output
        results :
            lea eax, line
            push eax
            call printf
            pop eax
            // Sum of Positive Numbers
            push sumPositive
            lea eax, positiveMSG
            push eax
            call printf
            pop ebx
            pop eax
            // Sum of Negative Numbers
            push sumNegative
            lea eax, negativeMSG
            push eax
            call printf
            pop ebx
            pop eax
            // Sum Zeros
            push sumZeros
            lea eax, zeroMSG
            push eax
            call printf
            pop ebx
            pop eax
    }

    return 0;
}